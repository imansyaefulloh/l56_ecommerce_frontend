import axios from 'axios'

export const state = () => ({
  categories: []
})

export const getters = {
  categories (state) {
    return state.categories
  }
}

export const mutations = {
  SET_CATEGORIES (state, categories) {
    state.categories = categories
  }
}

export const actions = {
  async nuxtServerInit ({ commit, dispatch }) {
    let response = await axios.get('http://l56-ecommerce.test/api/categories')
    
    commit('SET_CATEGORIES', response.data.data)

    if (this.$auth.loggedIn) {
      await dispatch('cart/getCart')
    }
  }
}